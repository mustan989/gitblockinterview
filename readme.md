# GetBlock Interview


## Task

Зарегистрироваться на GetBlock.io, используя API GetBlock создать сервис, выводящий адрес, баланс которого изменился (в любую сторону) больше остальных за последние сто блоков.

Получить номер последнего блока можно с помощью следующего метода:
https://getblock.io/docs/available-nodes-methods/ETH/JSON-RPC/eth_blockNumber/

А данные блока вместе с транзакциями через
https://getblock.io/docs/available-nodes-methods/ETH/JSON-RPC/eth_getBlockByNumber/


## Usage:

1. Add config file at `config/config.yaml` as example below:
    ```yaml
    client:
        url: https://eth.getblock.io/mainnet/ # rpc api url
        apiKey: string  # api key
    server:
        port: 9090      # app serving port
    ```
2. Run app `go run cmd/main.go`
3. Make GET request to `localhost:$port/top-changed`
4. See result

Adding `DEBUG=on` environment variable gives access to `/call/{method}?params=$param1,$paramN` method


## TODO

* Release unstable version
* Run options > config file. E.g: `./app --url=$URL --port=$PORT --apiKey=$KEY`


## Feedback

Что хорошо:
* Быстрая скорость работы
* Корректно проброшен контекст
* Реализовано API для получения данных в JSON
* Написан отдельный RPC клиент для запросов к ноде

Что можно исправить:
* Отсутствует Readme или пример конфига, корректную конфигурацию для запуска приходится выискивать в коде
  * upd: added readme
* Обработчики вместо отдельных функций вынесены в замыкания - вызовет проблемы при покрытии тестами, слишком длинные функции, слишком большая вложенность
* Не контролируется количество создаваемых горутин
* Обработчики не возвращают ошибки
* Если после вызова accts.LoadOrStore() значение хранимое по ключу будет изменено в другом потоке, это значение будет потеряно после вызова accts.Store()
* debug обработчик не отключается и позволяет выполнять любые методы с ключом - небезопасно
  * upd: added condition
* Не учтён газ
* Иногда при запросе в ответе возвращется адрес с изменением 0, хотя за секунду до этого в ответе был адрес с ненулевым изменением

Прочие замечания:
* Не совсем понятна причина, по которой ключ API необходимо прописать в конфигурацию в формате base64
  * comment: foolproof
* Тип HexInt описан, но не используется
  * upd: rm type
* Сущности парсятся полностью, но используется пара полей
  * upd: rm reundant fields

