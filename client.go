package getblockinterview

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

// NewRPCClient ctor
func NewRPCClient(url, apiKey string, opts ...RPCClientOption) *RPCClient {
	c := &RPCClient{url, apiKey, http.DefaultClient}
	for _, opt := range opts {
		opt(c)
	}
	return c
}

// RPCClient client for JSON-RPC
type RPCClient struct {
	url    string
	apiKey string
	client *http.Client
}

// Call calls remote procedure. result must be pointer type
func (c *RPCClient) Call(ctx context.Context, method string, result any, params ...any) error {
	b, err := json.Marshal(NewRequestBody(method, params...))
	if err != nil {
		return err
	}

	req, _ := http.NewRequestWithContext(ctx, http.MethodPost, c.url, bytes.NewBuffer(b))
	req.Header.Add("X-Api-Key", c.apiKey)
	req.Header.Add("Content-Type", "application/json")

	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	var res json.RawMessage
	resBody := &ResponseBody{Result: &res}

	if err := json.NewDecoder(resp.Body).Decode(&resBody); err != nil {
		return err
	}

	if resBody.Error != nil {
		return fmt.Errorf("%+v", resBody.Error)
	}

	return json.Unmarshal(res, result)
}

// RPCClientOption type to parametering RPClient at creation
type RPCClientOption func(c *RPCClient)

// WithHTTPClient func that changes default client
func WithHTTPClient(client *http.Client) RPCClientOption {
	return func(c *RPCClient) {
		c.client = client
	}
}

// NewRequestBody returns filled JSON-RPC request body
func NewRequestBody(method string, params ...any) *RequestBody {
	return &RequestBody{
		JSONRPC: "2.0",
		Method:  method,
		Params:  params,
		ID:      "getblock.io",
	}
}

// RequestBody JSON-RPC request body
type RequestBody struct {
	JSONRPC string `json:"jsonrpc"`
	Method  string `json:"method"`
	Params  []any  `json:"params"`
	ID      string `json:"id"`
}

// ResponseBody JSON-RPC response body
type ResponseBody struct {
	ID      string           `json:"id"`
	JSONRPC string           `json:"jsonrpc"`
	Result  *json.RawMessage `json:"result"`
	Error   any              `json:"error"`
}
