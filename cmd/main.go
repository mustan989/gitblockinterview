package main

import (
	"encoding/base64"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/labstack/echo/v4"
	"gitlab.com/mustan989/getblockinterview"
	"gopkg.in/yaml.v3"
)

func main() {
	cfg, err := parseConfig("config/config.yaml")
	if err != nil {
		log.Fatalf("error getting config: %v", err)
	}
	apiKey, err := base64.StdEncoding.DecodeString(cfg.Client.APIKey)
	if err != nil {
		log.Fatal("api key must be b64 encoded")
	}
	rpc := getblockinterview.NewRPCClient(cfg.Client.URL, string(apiKey))
	svc := getblockinterview.NewService(rpc)

	app := echo.New()

	app.GET("/top-changed", func(c echo.Context) error {
		n, err := strconv.Atoi(c.QueryParam("n"))
		if err != nil {
			n = 100
		}

		last, err := svc.GetLastBlock(c.Request().Context())
		if err != nil {
			return c.JSON(http.StatusInternalServerError, ResponseBody{Error: err})
		}

		res := Response{ActualAt: time.Now()}

		var accts sync.Map

		wg := sync.WaitGroup{}

		// process n blocks async
		for i := 0; i < n; i++ {
			wg.Add(1)
			go func(block *getblockinterview.Block) {
				defer wg.Done()

				blockWG := sync.WaitGroup{}
				blockWG.Add(len(last.Transactions))

				// process current block's transactions async
				for _, tHash := range last.Transactions {
					go func(hash string) {
						defer blockWG.Done()

						transaction, err := svc.GetTransaction(c.Request().Context(), hash)
						if err != nil {
							// TODO: handle
							return
						}

						from, fLoaded := accts.LoadOrStore(transaction.From, transaction.Value)
						if fLoaded {
							value, ok := from.(hexutil.Big)
							if !ok {
								// TODO: handle
								return
							}
							accts.Store(transaction.From, value.ToInt().Sub(value.ToInt(), transaction.Value.ToInt()))
						}

						to, tLoaded := accts.LoadOrStore(transaction.To, transaction.Value)
						if tLoaded {
							if fLoaded {
								value, ok := to.(hexutil.Big)
								if !ok {
									// TODO: handle
									return
								}
								accts.Store(transaction.From, value.ToInt().Add(value.ToInt(), transaction.Value.ToInt()))
							}
						}
					}(tHash)
				}

				blockWG.Wait()
			}(last)

			// exit if no parent
			if last.ParentHash == "" {
				break
			}

			last, err = svc.GetBlock(c.Request().Context(), last.ParentHash)
			if err != nil {
				// TODO: handle
				break
			}
		}
		wg.Wait()

		accts.Range(func(key, value any) bool {
			k, ok := key.(string)
			if !ok {
				// TODO: handle
				return true
			}
			val, _ := value.(hexutil.Big)
			if val.ToInt().CmpAbs(res.Value.ToInt()) == 1 {
				res.Address = k
				res.Value.ToInt().Set(val.ToInt())
				res.RawValue = res.Value.ToInt().Int64()
			}
			return true
		})

		res.CalcSecs = time.Now().Sub(res.ActualAt).Seconds()

		return c.JSON(http.StatusOK, ResponseBody{Response: &res})
	})

	// debug
	if strings.ToLower(os.Getenv("DEBUG")) == "on" {
		app.GET("/call/:method", func(c echo.Context) error {
			var res any
			var params []any
			for _, param := range strings.Split(c.QueryParam("params"), ",") {
				params = append(params, param)
			}
			if err := rpc.Call(c.Request().Context(), c.Param("method"), &res, params...); err != nil {
				return c.JSON(http.StatusInternalServerError, err.Error())
			}
			return c.JSON(http.StatusOK, res)
		})
	}

	log.Fatal(app.Start(fmt.Sprintf("%s:%d", cfg.Server.Host, cfg.Server.Port)))
}

// ResponseBody service json response structure
type ResponseBody struct {
	Response *Response `json:"response,omitempty"`
	Error    error     `json:"error,omitempty"`
}

// Response doc
type Response struct {
	ActualAt time.Time   `json:"actualAt"`
	Address  string      `json:"address"`
	Value    hexutil.Big `json:"value"`
	RawValue int64       `json:"rawValue"`
	CalcSecs float64     `json:"calcSecs"`
}

func parseConfig(path string) (*config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	b, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}
	cfg := &config{}
	if err := yaml.Unmarshal(b, cfg); err != nil {
		return nil, err
	}
	return cfg, nil
}

// config file structure
type config struct {
	Client struct {
		URL    string `yaml:"url"`
		APIKey string `yaml:"apiKey"`
	} `yaml:"client"`
	Server struct {
		Host string `yaml:"host"`
		Port int    `yaml:"port"`
	} `yaml:"server"`
}
