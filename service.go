package getblockinterview

import (
	"context"
	"fmt"
)

// NewService ctor
func NewService(client *RPCClient) *Service {
	return &Service{client}
}

// Service TODO: doc
type Service struct {
	rpc *RPCClient
}

// GetLastBlock returns chain head block
func (s *Service) GetLastBlock(ctx context.Context) (*Block, error) {
	var tag string
	if err := s.rpc.Call(ctx, "eth_blockNumber", &tag); err != nil {
		return nil, &serviceErr{"error getting last block", err}
	}
	if tag == "" {
		return nil, &serviceErr{"last tag unknown", nil}
	}

	var block Block
	if err := s.rpc.Call(ctx, "eth_getBlockByNumber", &block, tag, false); err != nil {
		return nil, &serviceErr{"error getting last block info", err}
	}
	return &block, nil
}

// GetBlock returns block info by given hash
func (s *Service) GetBlock(ctx context.Context, hash string) (*Block, error) {
	var block Block
	if err := s.rpc.Call(ctx, "eth_getBlockByHash", &block, hash, false); err != nil {
		return nil, &serviceErr{"error getting block by hash", err}
	}
	return &block, nil
}

// GetTransaction returns transaction info by given hash
func (s *Service) GetTransaction(ctx context.Context, hash string) (*Transaction, error) {
	var transaction Transaction
	if err := s.rpc.Call(ctx, "eth_getTransactionByHash", &transaction, hash); err != nil {
		return nil, &serviceErr{"error getting transaction", err}
	}
	return &transaction, nil
}

type serviceErr struct {
	Info string `json:"info"`
	Err  error  `json:"error,omitempty"`
}

func (se *serviceErr) Error() string {
	return fmt.Sprintf("%s: %s", se.Info, se.Err)
}
