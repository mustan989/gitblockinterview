package getblockinterview

import (
	"github.com/ethereum/go-ethereum/common/hexutil"
)

// Block TODO: doc
type Block struct {
	GasLimit     string   `json:"gasLimit"` // TODO: use
	GasUsed      string   `json:"gasUsed"`  // TODO:use
	ParentHash   string   `json:"parentHash"`
	Transactions []string `json:"transactions"`
}

// Transaction TODO: doc
type Transaction struct {
	From     string      `json:"from"`
	Gas      string      `json:"gas"`      // TODO: use
	GasPrice string      `json:"gasPrice"` // TODO: use
	To       string      `json:"to"`
	Value    hexutil.Big `json:"value"`
}
